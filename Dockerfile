FROM ubuntu:latest
RUN apt-get update && apt-get install -y tzdata
RUN apt-get install -y apache2
RUN apt-get install -y php
RUN apt-get install wget
RUN wget -O /tmp/wordpress.tar.gz https://wordpress.org/
